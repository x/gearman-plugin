This project is no longer maintained on OpenDev.

The contents of this repository are still available in the Git
source code management system.  To see the contents of this
repository before it reached its end of life, please check out the
previous commit with "git checkout HEAD^1".

The Jenkins Gearman plugin development has been migrated to the Jenkins
community and is being continued at:

https://github.com/jenkinsci/gearman-plugin/
